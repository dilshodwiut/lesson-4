// Assignment 4

function charReverseHandler(str) {
  return str.split("").reduce(function (acc, curr) {
    // capital letter
    if (curr.charCodeAt() <= 90) {
      return acc + curr.toLowerCase();
    }
    // small letter
    else if (curr.charCodeAt() >= 97) {
      return acc + curr.toUpperCase();
    }
  }, "");
}

function createListHandler() {
  return "https://todo-dilshod.netlify.app/";
}

function findDublicatedHandler(arr) {
  Array.from(new Set(arr)).forEach((item) => {
    const index = arr.findIndex((el) => el == item);
    arr.splice(index, 1);
  });
  return Array.from(new Set(arr));
}

function unionHandler(arr1, arr2) {
  return arr1.filter(function (item) {
    return arr2.includes(item) ? item : null;
  });
}

function stripFalsyHandler(arr) {
  return arr.filter(function (item) {
    return !!item;
  });
}

function sortByAlphabeticalHandler(str) {
  return str
    .toLowerCase()
    .split("")
    .sort(function (a, b) {
      return a.charCodeAt() - b.charCodeAt();
    })
    .join();
}
